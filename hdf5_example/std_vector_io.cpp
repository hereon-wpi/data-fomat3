//
// (c) Copyright 2017 DESY,ESS
//
// This file is part of h5cpp.
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty ofMERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301 USA
// ===========================================================================
//
// Author: Eugen Wintersberger <eugen.wintersberger@desy.de>
// Created on: Oct 07, 2017
//
#include <h5cpp/h5cpp.hpp>
#include <iostream>
#include <vector>
#include <numeric>
#include <string>
using namespace hdf5;

//
// h5cpp has native support for std::vector IO. This example shows how to do it.
//

using DataType2 = std::vector<std::vector<std::vector<int>>>;

//DataType2 write_data2;

struct Data_Structure_t{
  int NumberofDevices;
  std::vector<int> NumberofAttributes;
  int TotalNumberofAttributes;
  std::vector<int> NumberofValues;
  int TotalNumberofValues;
};
Data_Structure_t Data_Structure;

 char Device_Names[5][2];
 char Attribute_Names[16][2];
 //Device_Names[0][0] = '1';
 //Device_Names[1][0] = '2';
 //Device_Names[2][0] = '3';
 //Device_Names[3][0] = '4';
 //Attribute_Names[0][0] = 'a';
 //Attribute_Names[1][0] = 'b';
 //Attribute_Names[2][0] = 'c';
 //Attribute_Names[3][0] = 'd';
 //Attribute_Names[4][0] = 'e';
 //Attribute_Names[5][0] = 'f';
 //Attribute_Names[6][0] = 'g';
 //Attribute_Names[7][0] = 'h';
 //Attribute_Names[8][0] = 'i';
 //Attribute_Names[9][0] = 'j';
 //Attribute_Names[10][0] = 'k';
 //Attribute_Names[11][0] = 'l';
 //Attribute_Names[12][0] = 'm';
 //Attribute_Names[13][0] = 'n';
 //Attribute_Names[14][0] = 'o';


void creathdf5tree(std::vector<std::vector<std::vector<int>>> *pwdata, Data_Structure_t *pData_Structure)
{
  file::File f = file::create("std_vector_io.h5",file::AccessFlags::Truncate);
  node::Group root_group = f.root();
  std::cout <<  pData_Structure->NumberofDevices << std::endl;
  std::cout <<  pData_Structure->TotalNumberofAttributes << std::endl;
  for(int i=0 ;i < pData_Structure->NumberofDevices ;i++)
  {
        std::map<int, node::Group> Device;
        node::Group Dev(root_group, &Device_Names[i][0]);
	std::cout << Device_Names[i][0] << std::endl;
        Device[i] = Dev;
        for(int j = 0; j < pData_Structure->NumberofAttributes[i]; j++)
        {
                        //std::map<int, node::Group> Device;
                        //node::Group Dev(root_group, &Device_Names[i][0]);
                        //Device[i] = Dev;
                        std::map<int, node::Group> Attribute;
                        node::Group Attr(Device[i], &Attribute_Names[j][0]);
                        Attribute[j] = Attr;
                        node::Dataset dataset2(Attribute[j],"data",datatype::create<DataType2>(), dataspace::create(pwdata[i][j]));
                        dataset2.write(pwdata[i][j]);
        }
  }
}


void InitData_Structure(int NumberofDevices, std::vector<int> NumberofAttributes, std::vector<int> NumberofValues){
     Data_Structure.NumberofDevices = NumberofDevices;
     Data_Structure.NumberofAttributes = NumberofAttributes;
     Data_Structure.TotalNumberofAttributes = 0;
     Data_Structure.TotalNumberofAttributes = accumulate(NumberofAttributes.begin(), NumberofAttributes.end(), Data_Structure.TotalNumberofAttributes);
     Data_Structure.NumberofValues = NumberofValues;
     Data_Structure.TotalNumberofValues = 0;
     Data_Structure.TotalNumberofValues = accumulate(NumberofValues.begin(), NumberofValues.end(), Data_Structure.TotalNumberofValues);
}

void Fill_Data_Vector(std::vector<std::vector<std::vector<int>>> *pwdata, Data_Structure_t *pData_Structure)
{

 for (int i = 0; i < pData_Structure->NumberofDevices; i++)
 {
        std::vector<std::vector<int>> wdata2d;
        for (int j = 0; j < pData_Structure->NumberofAttributes[i]; j++)
        {
                 std::vector<int> wdata1d;
                 for(int k = 0; k < pData_Structure->NumberofValues[j]; k++)
                 {
                          wdata1d.push_back(i*j*k);
                 }
                 wdata2d.push_back(wdata1d);
        }
        pwdata->push_back(wdata2d);
  }

}

using DataType = std::vector<int>;
//using DataType2 = std::vector<std::vector<std::vector<int>>>;

int main()
{
  DataType2 write_data2;
  int NumberofDevices_local = 4;
  std::vector<int> NumberofAttributes_local = {4, 6, 3, 2};
  std::vector<int> NumberofValues_local = {1, 2, 3, 5, 8, 13, 6, 8, 8, 1, 3, 5, 7, 5, 2};
  InitData_Structure(NumberofDevices_local, NumberofAttributes_local, NumberofValues_local);

  //
  // writing data to a dataset in the file
  //
 // DataType write_data{1,2,3,4};
 // file::File f = file::create("std_vector_io.h5",file::AccessFlags::Truncate);
 // node::Group root_group = f.root();
 Fill_Data_Vector(&write_data2, &Data_Structure);

 ///Print Data Vector ///
 for(int i = 0; i < Data_Structure.NumberofDevices;i++)
 {
	for(int j = 0; j < Data_Structure.NumberofAttributes[i];j++)
	{
		for(int k = 0; k < Data_Structure.NumberofValues[j];k++)
		 {
			std::cout << write_data2[i][j][k] <<";";
		 }
		 std::cout << std::endl;
	}
	std::cout << std::endl;
 }

  // retrieving the data back from disk

 //DataType read_data(static_cast<size_t>(dataset.dataspace().size())); //allocate enough memory
 //dataset.read(read_data);

  //
  // print the data
  //
  //std::for_each(read_data.begin(),read_data.end(),
  //            [](int value) { std::cout<<value<<"\t"; });
  //std::cout<<std::endl;
 // file::File f = file::create("std_vector_io.h5",file::AccessFlags::Truncate);
  //node::Group root_group = f.root();
 // std::map<int, node::Group> Device;
 //char Device_Names[5][2];
 //char Attribute_Names[16][2];
 Device_Names[0][0] = '1';
 Device_Names[1][0] = '2';
 Device_Names[2][0] = '3';
 Device_Names[3][0] = '4';

 Attribute_Names[0][0] = 'a';
 Attribute_Names[1][0] = 'b';
 Attribute_Names[2][0] = 'c';
 Attribute_Names[3][0] = 'd';
 Attribute_Names[4][0] = 'e';
 Attribute_Names[5][0] = 'f';
 Attribute_Names[6][0] = 'g';
 Attribute_Names[7][0] = 'h';
 Attribute_Names[8][0] = 'i';
 Attribute_Names[9][0] = 'j';
 Attribute_Names[10][0] = 'k';
 Attribute_Names[11][0] = 'l';
 Attribute_Names[12][0] = 'm';
 Attribute_Names[13][0] = 'n';
 Attribute_Names[14][0] = 'o';

 std::cout <<  Data_Structure.NumberofDevices << std::endl;
 std::cout <<  Data_Structure.TotalNumberofAttributes << std::endl;

  file::File f = file::create("std_vector_io.h5",file::AccessFlags::Truncate);
  node::Group root_group = f.root();
  //std::cout <<  pData_Structure->NumberofDevices << std::endl;
  //std::cout <<  pData_Structure->TotalNumberofAttributes << std::endl;
  for(int i=0 ;i < Data_Structure.NumberofDevices ;i++)
  {
        std::map<int, node::Group> Device;
        node::Group Dev(root_group, &Device_Names[i][0]);
        //std::cout << Device_Names[i][0] << std::endl;
        Device[i] = Dev;
        for(int j = 0; j < Data_Structure.NumberofAttributes[i]; j++)
        {
                        //std::map<int, node::Group> Device;
                        //node::Group Dev(root_group, &Device_Names[i][0]);
                        //Device[i] = Dev;
                        std::map<int, node::Group> Attribute;
                        node::Group Attr(Device[i], &Attribute_Names[j][0]);
                        Attribute[j] = Attr;
                        node::Dataset dataset2(Attribute[j],"data",datatype::create<DataType2>(), dataspace::create(write_data2[i][j]));
                        dataset2.write(write_data2[i][j]);
        }
  }

 
  return 0;
}

